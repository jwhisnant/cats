#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: neo4j_loader.py
Author: James Whisnant
Email: jwhisnant@gmail.com
Github: https://github.com/jwhisnant
Description: Load some data into neo4j

    ("`-''-/").___..--''"`-._
     `6_ 6  )   `-.  (     ).`-.__.`)
     (_Y_.)'  ._   )  `._ `. ``-..-'
   _..`--'_..-_/  /--'_.' ,'
  (il),-''  (li),'  ((!.-'    Felix Lee

https://user.xmission.com/~emailbox/ascii_cats.htm

"""
import csv
import os
import logging
import itertools
import getpass
import argparse

from progress.spinner import Spinner

from py2neo import Node, Relationship
from py2neo import Graph
from py2neo.error import GraphError

from py2neo import watch

# logging
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def contiguous_sample(graph, number, name='Heathrow Terminal 4'):
    """Generate a subraph of a smaller number from
    the original graph

    Parameters
    ----------
    graph : a Graph object
    number:  an integer
    name: optional (name of station to expand from)

    Returns
    -------
    recordlist: a py2neo recordlist

    """
    query = """MATCH (n1:Station{name:'%s'})-[r*1..200]->(n2)
    RETURN DISTINCT n1, n2 LIMIT %s;""" % (name, number)
    recordlist = graph.cypher.execute(query)
    return recordlist


def _create(fqn, name):
    """Create a dictionary of "ungraphed" nodes

    Parameters
    ----------
    fqn : a csv file name with station info
    name : the title of a node

    Returns
    -------
    adict: a dictionary of node objects

    """
    adict = {}
    with open(fqn) as my_file:
        reader = csv.reader(my_file)
        for row in reader:
            if row:
                number, station = row
                number = int(number.strip())
                node = Node(name.strip().title(), name=station.strip(), number=number)
                adict[number] = node
    return adict


def _create_relationship(fqn, name, adict):
    """Create an "unbound" dictionary of neo4j relationships

    Parameters
    ----------
    fqn : a csv filename of relationships between stations
    name : title of the nodes
    adict : a dictionary which (optionally) has nodes

    Returns
    -------
    outdict: a dictionary of neo4j relationships and nodes

    """
    outdict = {}

    with open(fqn) as my_file:
        reader = csv.reader(my_file)
        for row in reader:
            if row:
                id1, id2 = row
                id1 = int(id1.strip())
                id2 = int(id2.strip())

                node1 = adict.get(id1)
                node2 = adict.get(id2)

                lex = tuple(sorted(list((id1, id2))))
                if lex not in outdict:
                    outdict[lex] = {}
                    outdict[lex]['rels'] = []

                relationship = Relationship(node1, name, node2)
                if relationship not in outdict[lex]['rels']:
                    outdict[lex]['rels'].append(relationship)

                relationship = Relationship(node2, name, node1)  # and the reverse
                if relationship not in outdict[lex]['rels']:
                    outdict[lex]['rels'].append(relationship)

                outdict[lex]['nodes'] = [node1, node2]

    return outdict


def add_to_graph(graph, reldict):
    """Add nodes and relationships to a graph.

    Parameters
    ----------
    graph : a py2neo graph
    reldict : a complete dictionary representation of all nodes

    Returns
    -------
    reldict, the_copy: a dictionary of nodes and relationships, copy of reldict

    """

    the_copy = reldict.copy()

    bar = Spinner('Adding nodes and relationships: ')

    for key in reldict:
        for node in reldict[key]['nodes']:
            graph.create(node)
            bar.next()

        for rel in reldict[key]['rels']:
            graph.create(rel)
            bar.next()

    print # clear the spinner output
    return reldict, the_copy


def recordlist_to_subgraph(recordlist, graph, the_copy):
    """TODO: Take a recordlist of n1 and n2 and convert that into a contigous subgraph
    Recognize that n2 holds all the unique nodes we need so we can ignore n1.

    Parameters
    ----------
    recordlist : a py2neo recordlist
    graph : a complete graph of all nodes and relationships

    Returns
    -------
    graph, new_dict: a contingous subgraph, a dictionary representation of the subgraph
    """

    ids = []

    for value in recordlist:
        ids.append(value.n2.properties['number'])

    keys = [tuple(x) for x in itertools.permutations(ids, 2)]
    """find the_copy keys of interest based on all permutations"""

    new_dict = {}
    for key in keys:
        if key in the_copy:
            new_dict[key] = the_copy[key].copy()

    graph, new_dict = _delete(graph, new_dict, the_copy)
    return graph, new_dict


def _delete(graph, new_dict, the_copy):
    """Delete nodes and paths that are not in our "contiguous" subgraph

    Parameters
    ----------
    graph : a neo2py graph
    new_dict: a dictionary defining a continguous set of nodes
    the_copy : a dictionary of keys with rels and nodes

    Returns
    -------
    graph, new_dict: a graph, and new_dict

    """
    all_rels = []
    all_nodes = []

    for key in the_copy:
        if key not in new_dict:
            rels = the_copy[key]['rels']
            nodes = the_copy[key]['nodes']

            for rel in rels:
                all_rels.append(rel)

            for node in nodes:
                all_nodes.append(node)

    """ try to delete rels first """
    for rel in all_rels:
        graph.delete(rel)

    for node in all_nodes:
        try:
            graph.delete(node)
        except GraphError as err:
            logger.debug('%s:%s' % (node.properties.get('name'), err))

    return graph, new_dict


def _parser(parser=None):
    if not parser:
        parser = argparse.ArgumentParser(description='Arguments for Building the Underground.')
        parser.add_argument('--stations', type=int, nargs='?', help='Number of stations to create (default ALL)')
        parser.add_argument('--path', type=str, nargs='?', help='path to data files')
        #parser.add_argument('--station', type=str, nargs='?', help='Station name to prune from', default='Baker Street')
        parser.add_argument('--origin', type=str, nargs='?', help='Station name to prune from', default='Heathrow Terminal 4')

        parser.add_argument('--watch', dest='watch_http', help='watch requests', action='store_true')
        parser.set_defaults(watch_http=False)

    return parser


def main(num=None, parser=None):
    """TODO: Docstring for main.

    Parameters
    ----------
    num : a number of nodes for a subgraph, optional

    Returns
    -------
    tuple: graph, reldict
        graph: a py2neo graph (full or subgraph)
        reldict: a dictionary representation of the graph

    """

    parser=_parser(parser)
    args = parser.parse_args()

    if args.watch_http:
        watch('httpstream')

    """ determine path """
    path = args.path or '/home/%s/projects/cats/data' % (getpass.getuser())
    #num = args.number or None
    num = args.stations or None

    """ create the station nodes"""
    stations = os.path.join(path, 'tfl_stations.csv')
    stations_dict = _create(stations, 'station')

    """ create our relationships """
    connections = os.path.join(path, 'tfl_connections.csv')
    reldict = _create_relationship(connections, 'Connected', stations_dict)

    logger.info('Creating %s nodes' % (len(stations_dict)))

    """ cleanup """
    graph = Graph("http://neo4j:neo4j!@localhost:7474/db/data/")
    graph.delete_all()

    """ create 302 nodes """
    reldict, the_copy = add_to_graph(graph, reldict)

    """ prune graph if necessary """

    if num:
        num = int(num)
        recordlist = contiguous_sample(graph, number=num, name=args.origin)
        logger.info('Pruning to %s contiguous nodes from %s' % (num, args.origin))

        graph, reldict = recordlist_to_subgraph(recordlist, graph, the_copy)

    return graph, reldict

if __name__ == '__main__':
    main()
