#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: csv_loader.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
    Load a csv file into neo4j

                                     ,
              ,-.       _,---._ __  / \
             /  )    .-'       `./ /   \
            (  (   ,'            `/    /|
             \  `-"             \'\   / |
              `.              ,  \ \ /  |
               /`.          ,'-`----Y   |
              (            ;        |   '
              |  ,-.    ,-'         |  /
              |  | (   |        hjw | /
              )  |  \  `.___________|/
              `--'   `--'

https://user.xmission.com/~emailbox/ascii_cats.htm

"""

import argparse
import csv
import logging
import os
import time

from py2neo import Graph

# logging
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def bidirectionalize(csv_in):
    """TODO: Docstring for bidirectionalize.

    Parameters
    ----------
    csv_in : fully qualified path, a string

    Returns
    -------
    csv rows
    """
    lines = []
    with open(csv_in, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            data = (int(row[0]), int(row[1]))
            data1 = (int(row[1]), int(row[0]))

            lines.append(data)
            lines.append(data1)
    return sorted(list(set(lines)))


def write_csv(csv_out, lines):
    """TODO: Docstring for write_csv.

    Parameters
    ----------
    csv_out : fqn
    lines : list of rows to write

    Returns
    -------
    lines
    """

    with open(csv_out, 'w') as csvfile:
        writer = csv.writer(csvfile)
        for row in lines:
            str_row = (str(row[0]), str(row[1]))
            writer.writerow(str_row)
    return lines


def _execute(query):
    start = time.time()
    logger.info(query)
    recordlist = graph.cypher.execute(query)
    logger.info('Query took %.03f seconds' % (time.time() - start))
    return recordlist


def load_stations(station_fqn):
    """TODO: Docstring for load_stations.

    Parameters
    ----------
    station_fqn : fqn, string

    Returns
    -------
    recordlist
    """

    query = """
    LOAD CSV FROM 'file:///%s' as line CREATE
    (:Station {number:toInt(line[0]), name:line[1] } ) ;
    """ % (station_fqn)
    return _execute(query)


def load_relationships(rel_fqn):
    """TODO: Docstring for load_relationships.

    Parameters
    ----------
    rel_fqn : TODO

    Returns
    -------
    TODO

    """

    query = """
    LOAD CSV FROM 'file:///%s' as line
    MATCH (n1:Station{number:toInt(line[0])} ),
    (n2:Station{number:toInt(line[1]) })
    CREATE (n1)-[:Connected]->(n2) ;
    """ % (rel_fqn)
    return _execute(query)


def main(csv_in, csv_out, station_fqn):
    """TODO: Docstring for main.

    Parameters
    ----------
    csv_in : fqn, string
    csv_out : fqn, string

    Returns
    -------
    TODO

    """
    index()
    lines = bidirectionalize(csv_in)
    lines = write_csv(csv_out, lines)
    recordlist_stations = load_stations(station_fqn)
    recordlist_relationships = load_relationships(csv_out)

def index():
    query="""CREATE INDEX ON :Station(number)"""
    _execute(query)

def clear():
    graph = Graph("http://neo4j:neo4j!@localhost:7474/db/data/")
    graph.delete_all()
    return graph

if __name__ == '__main__':
    # XXX TODO: make project "path" aware 2016/03/22  (jwhisnant)
    graph=clear()

    csv_in = '/home/jwhisnant/projects/cats/data/tfl_connections.csv'
    csv_out = '/home/jwhisnant/projects/cats/data/tfl_bi_connections.csv'

    station_fqn = '/home/jwhisnant/projects/cats/data/tfl_stations.csv'

    main(csv_in=csv_in,
         csv_out=csv_out,
         station_fqn=station_fqn
         )
