#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
File: game.py
Author: James Whisnant
Email: jwhisnant@gmail.com
Github: https://github.com/jwhisnant
Description: Play the cat game

                \`*-.                   
                 )  _`-.                
                .  : `. .               
                : _   '  \              
                ; *` _.   `*-._         
                `-.-'          `-.      
                  ;       `       `.    
                  :.       .        \   
                  . \  .   :   .-'   .  
                  '  `+.;  ;  '      :  
                  :  '  |    ;       ;-.
                  ; '   : :`-:     _.`* ;
         [bug] .*' /  .*' ; .*`- +'  `*'
               `*-*   `*-*  `*-*'       

https://user.xmission.com/~emailbox/ascii_cats.htm

"""
import sys
import time
import logging
import random

from progress.bar import Bar
from collections import Counter
from py2neo import Node, Relationship
import neo4j_loader as loader

# logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Game(object):

    """
    I represent the game state
    """

    def __init__(self, graph, num_walkers, parser):
        """
        shall we play a game?
        """
        self.graph = graph
        self.num_walkers = num_walkers
        self.parser = parser

        self.humans = {}
        self.cats = {}
        self.removed = {}
        self.futile = {}

        self.found_at = set((0,))

        self.rounds = 0

        # if nobody can move, the game is over ...
        self.stuck = []

        self.subraphs = []

    def _setup(self):
        """
        randomize and place humans and cats
        """

        self._randomize(self.num_walkers)

        # setup the board
        for num, human in self.humans.items():
            self.place_walker(human)
            self.associate_walker(human)

        for num, cat in self.cats.items():
            self.place_walker(cat)
            self.associate_walker(cat)

    def check_isolated(self, loc):
        """
        check for isolated items
        return 'Hopeless' nodes
        """
        sub, recordlist = self.subgraph(loc)
        walkers = self.find_walkers_in_graph(sub)
        failed = self.hopeless(walkers)
        return failed

    def hopeless(self, walkers):
        """
        mark all hopeless nodes as hopeless
        import ipdb; ipdb.set_trace()  # XXX BREAKPOINT
        """

        out = []
        msgs = []
        for key, nodes in walkers.items():
            if len(nodes) == 1:
                node = nodes[0]
                self.reassign(node, 'Hopeless')

                """ find the matching """
                name1 = node.properties.get('name')
                if name1.startswith('H'):
                    other = self.cats.get(key)

                if name1.startswith('C'):
                    other = self.humans.get(key)

                self.reassign(other.node, 'Hopeless')
                out.append((node, other.node))

                msgs.append(name1)
                msgs.append(other.node.properties.get('name'))

        if msgs:
            logger.info('Hopeless: %s' % (','.join(msgs)))
        return out

    def reassign(self, node, rel_name, station=None):
        """
        change the name of a relationship
        """
        for rel in node.match_outgoing(): # should be one
            if not station:
                station = rel.end_node
            self.graph.delete(rel)
            relationship = Relationship(node, rel_name, station)

            self.graph.create(relationship)
        return node

    def subgraph(self, loc):
        """
        create a subgraph to find things
        that are isolated
        """
        query = """MATCH (n{name:'%s'})-[r*]->(n2)
        RETURN DISTINCT n,r,n2;""" % (loc)
        recordlist = self.graph.cypher.execute(query)
        sub = recordlist.to_subgraph()
        return sub, recordlist

    def find_walkers_in_graph(self, subgraph):
        """
        create a dictionary of walkers in a  subgraph
        those with a len=1 are isolated
        """
        walkers = {}

        for station in subgraph.nodes:
            rels = station.match_incoming(rel_type='Present')

            for rel in rels:
                node = rel.start_node
                key = int(node.properties.get('number'))
                if key not in walkers:
                    walkers[key] = []
                walkers[key].append(node)

        return walkers

    def moves_made(self):
        out = []
        for id, human in self.humans.items():
            out.append(human.did_move)

        for id, cat in self.cats.items():
            out.append(cat.did_move)

        return sum(out)

    def _is_game_over(self):
        """
        the game is over when we have
        reached max iterations
        """

        if self.moves_made() == 0:
            logger.info('\nNobody can move.')
            self.game_over()

        if not self.humans:
            self.game_over()

        if not self.humans:
            self.game_over()

        # not over
        self.moves = 0

    def calc_average(self):
        """
        return the average of how long it took to find cats
        """
        nums = sorted(list(self.found_at))
        first = nums[::2]
        second = nums[1::2]

        how_many = []
        for num, sec in enumerate(second):
            how_many.append(sec - first[num])

        if not how_many:
            return 'undefined'

        avg = sum(how_many) / float(len(how_many))
        return '%.2f' % (avg)

    def _choices(self, num_walkers):
        """
        find adjacent possible moves
        """
        alist = []
        for node in self.graph.find('Station'):
            outgoings = node.match_outgoing(rel_type='Connected')
            for out in outgoings:
                outnode = out.end_node
                alist.append(node.properties.get('number'))
                alist.append(outnode.properties.get('number'))
        return sorted(set(alist))

    def _randomize(self, num_walkers):
        """
        randomize placement 2n things
        """
        # places = {}
        choices = self._choices(num_walkers)
        # choices = [x[0] for x in self.reldict.keys()] + [x[1] for x in self.reldict.keys()]
        # choices = tuple(set(choices))

        for num, walker in enumerate(range(0, num_walkers)):

            first, second = random.sample(choices, 2)

            human = Human(number=num, name='Human', loc=first)
            cat = Cat(number=num, name='Cat', loc=second)

            self.humans[num] = human
            self.cats[num] = cat

    def place_walker(self, walker):
        """
        place a walker object on the self.graph
        """
        my_name = walker.name[0].upper() + str(walker.number)
        node = Node(walker.name.strip().title(),
                    name=my_name,
                    number=walker.number.strip())

        self.graph.create(node)
        walker.node = node

    def associate_walker(self, walker):
        station = self.graph.find_one('Station', 'number', walker.loc)
        relationship = Relationship(walker.node, 'Present', station)
        self.graph.create(relationship)

        return relationship

    def close_station(self, station, human, cat):
        """
        write a log message
        station, humans, cat are node objects
        """
        loc = station.properties.get('name')
        hnum = human.properties.get('number')
        cnum = cat.properties.get('number')

        # self.reassign(human, 'Partying', station=station)
        # self.reassign(cat, 'Partying', station=station)
        relationship = Relationship(human, 'Found', cat)

        print '\nOwner %s found cat %s - %s is now closed.' % (hnum, cnum, loc)

        connected = [x for x in station.match(rel_type='Connected')]

        """ find self and adjacents to prepare for hopeless things"""
        adjacents = []
        for rel in connected:
            adjacents.append(rel.end_node)
            adjacents.append(rel.start_node)

        adjacents = set(adjacents)

        self.graph.delete(*connected)

        if self.parser.parse_args().hopeless:

            start = time.time()
            logger.info('Hopeless search started at %s'%(time.ctime()) )

            for adjacent in adjacents:
                sad_pairs = self.check_isolated(
                    adjacent.properties.get('name'))

                for n1, n2 in sad_pairs:
                    one = n1.properties.get('number')
                    two = n2.properties.get('number')
                    assert one == two

                    self.remove_matches(one, two, mode='hopeless')
            logger.info('Hopeless search took %.02f seconds' %
                        (time.time() - start))

        self.remove_matches(hnum, cnum)

    def party_time(self, station, cat, human):
        """
        change the Connected relation type to be Party
        """

    def remove_matches(self, hnum, cnum, mode='matched'):
        if hnum != cnum:
            raise

        if mode == 'matched':
            self.removed[hnum] = [
                self.humans.get(int(hnum)), self.cats.get(int(cnum))]

        if mode == 'hopeless':
            self.futile[hnum] = [
                self.humans.get(int(hnum)), self.cats.get(int(cnum))]

        try:
            del self.humans[int(hnum)]
        except KeyError:
            pass

        try:
            del self.cats[int(cnum)]
        except KeyError:
            pass

    def _mill(self, walkers):
        for num, walker in walkers.items():
            walker = walker.move()

            # delete old connections if not stuck
            if not walker.stuck:

                # delete old location
                connected = [x for x in walker.node.match(rel_type='Present')]
                self.graph.delete(*connected)

                # associate walker with new location
                rel = self.associate_walker(walker)

            if walker.stuck:
                id = int(walker.node.properties.get('number'))

                if isinstance(walker, Cat):
                    del self.cats[id]

                if isinstance(walker, Human):
                    del self.humans[id]

        return walkers

    def mill_around(self):
        """
        humans must go last
        because they move and search
        """
        # mill around
        self._mill(self.cats)
        self._mill(self.humans)

    def game_over(self):
        """
        print ending message
        """
        out = '''\
Total number of cats: %s
Number of cats found: %s
Average number of movements required to find a cat: %s''' % (
            self.num_walkers,
            len(self.removed),
            self.calc_average())

        print out
        sys.exit()


class Walker(object):

    """
    I walk around a neo4j self.graph
    """

    def __init__(self, number, name, loc):
        self.number = str(number)
        self.name = name  # XXX TODO: no unicode for you 2015/10/18  (james)
        self.loc = loc

        # we shall defined used as the station we are leaving from
        self.used = Counter()
        self.node = None
        self.stuck = False

        self.found = None

        self.did_move = 0

    def __repr__(self):
        return '<%s: num:%s, loc=%s, name=%s>' % (self.__class__.__name__, self.number, self.loc, self.name)

    def get_current_station(self):
        node = self.node
        connected = [x for x in node.match(rel_type='Present')]

        station = connected[0].end_node
        return station

    def move(self):
        """
        remove the relationship from a walker and a station
        find a new station
        create a relationship to new station
        """
        self.did_move = 0
        station = self.get_current_station()

        next_station = self.get_next_node(station)

        if not next_station:
            msg = '\nNo adjacent stations: %s for %s to leave\n' % (
                station.properties, self)
            self.stuck = True
            logger.warning(msg)

        if next_station:
            self.loc = next_station.properties.get('number')
            # self.used.update([station, next_station])
            self.used.update([self.loc, ])
            # print 'next station', self.loc, 'where have I been',
            # sorted(self.used.items())
            self.did_move = 1

        return self

    def _get_adjacents(self, station):
        """
        Find outgoing station connections
        """
        adjacents = list(set(station.match_outgoing(rel_type='Connected')))
        return adjacents

    def get_next_node(self, station):
        """
        find a connected station that is not this station
        This returns a random adjacent node
        """
        adjacents = self._get_adjacents(station)

        if not adjacents:
            return None

        choice = random.choice(adjacents)
        return choice.end_node


class Human(Walker):

    """
    Humans are walkers that remember their paths
    """

    def __call__(self):
        self.move()
        return self

    def found_cat(self):
        """
        check for a cat in my square
        return a cat node if there is one ...
        """
        target = 'C' + self.number

        # get station
        station = list(self.node.match(rel_type='Present'))[0].end_node

        # get other things at the station
        things = list(station.match(rel_type='Present'))

        for rel in things:
            start = rel.start_node
            start.properties.pull()

            if start.properties.get('name') == target:
                """ create a found relationship between cat and human"""
                relationship = Relationship(self.node, 'Found', start)
                self.node.graph.create(relationship)
                return station, start

        return None, None

    def be_smart(self, adjacents):
        """
        humans are smart
        return adjacencies that are minimium
        #XXX TODO: seems like there should be a shorter way ... 2015/10/21  (james)
        """
        # get a list of stations connected to the adjacenies

        # print adjacents
        possibles = {}
        for rel in adjacents:
            tentative_station = rel.end_node
            station_number = tentative_station.properties.get('number')

            if station_number not in possibles:
                possibles[station_number] = []

            possibles[station_number].append((tentative_station, rel))

        prune = []
        if self.used:
            common = self.used.most_common()
            random.shuffle(common)
            start = common[0][1]

            for station, number in common:
                if start == number:
                    prune.append(station)

        for thing in set(prune):
            if thing in possibles:
                del possibles[thing]

        out = []
        for id in possibles:
            for station, rel in possibles[id]:
                out.append(rel)

        if not out:  # no moves, then all moves
            out = adjacents
        diff = len(adjacents) - len(out)
        if diff:
            msg = '\nPruned %s adjacencies %s' % (diff, repr(self))
            logger.debug(msg)
        #print out
        return out

    def get_next_node(self, station):
        """
        humans are smart, examine our path list and choose wisely
        after we choose wisely, we can use the Walker's move method
        """
        #moving_station = station
        adjacents = self._get_adjacents(station)
        adjacents = self.be_smart(adjacents)

        if not adjacents:
            return None

        # this can happen with "smart" things
        if len(adjacents) == 1:
            moving_station = adjacents[0].end_node
            return moving_station

        moving_rel = random.choice(adjacents)
        moving_station = moving_rel.end_node

        assert station is not moving_station
        # XXX FIXME: desc 2015/11/27  (james)
        return moving_station


class Cat(Walker):

    """
    doc
    """

    def __call__(self):
        self.move()
        return self


def _parser(parser=None):
    """ get the parser from the loader (if we don't have one) """
    if not parser:
        parser = loader._parser()

        """ add walkers arg here """
        parser.add_argument(
            '--walkers', type=int, nargs='?', help='Number of walkers', default=1)

        parser.add_argument(
            '--hopeless', dest='hopeless', help='prune hopeless', action='store_true')
        parser.set_defaults(hopeless=False)

    return parser


def main(parser=None):

    parser = _parser(parser)
    args = parser.parse_args()

    if args.hopeless:
        logger.warning(
            'This option may cause your machine to run out of memory and crash for large graphs.')
        logger.warning(
            'It is suggested to use this on graphs less than 15 nodes.')

    graph, reldict = loader.main(parser=parser)
    logger.info('Tube initialized.  Mind the gap.')

    cat_game = Game(graph=graph, num_walkers=args.walkers, parser=parser)

    # setup
    cat_game._setup()
    logger.info('%s humans and cats have been unleashed on the Underground!' %
                (args.walkers))

    found_total = []

    # iterate
    amount = 10 * 10 * 10 # should be 100k really ..
    bar = Bar('Chasing cats', max=amount)

    for num in range(1, amount):
        cat_game.rounds = num
        bar.next()
        cat_game.mill_around()

        # find cats
        found = []
        for hnum, human in cat_game.humans.items():
            station, cat = human.found_cat()
            if station and cat:
                found.append((station, human, cat))

        if found:
            cat_game.found_at.add(num)
            found_total.extend(found)

        # close stations
        for station, human, cat in found:
            cat_game.close_station(station, human.node, cat)

        # process stuck things
        for hnum, human in cat_game.humans.items():
            if human.stuck:
                del cat_game.humans[hnum]

        for cnum, cat in cat_game.humans.items():
            if cat.stuck:
                del cat_game.cats[cnum]

        cat_game._is_game_over()

    cat_game.game_over()

if __name__ == '__main__':
    main()
